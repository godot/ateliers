Pour participer à la documentation des ateliers ou envoyer vos scènes et vos codes, il vous faut créer un compte ou vous connecter:    
https://framagit.org/users/sign_in?redirect_to_referer=yes

Donnez votre pseudo à l'un des organisateurs afin qu'il vous ajoute au groupe Godot pour pouvoir éditer le contenu des ateliers, créer des dépots afin d'envoyer les fichiers de vos jeux, ou collaborer aux projets déjà existants dans le groupe. . .  

Les différents projets seront également stoqués ici pour lecture et enrichissement (pull/push). Vous pouvez donc y laisser les votres si vous souhaitez partager et avancer à plusieurs dessus...

* Jeu princesse sorcière (2D) : https://framagit.org/godot/JeuPrincesse (premier jeu basique présenté lors du premier atelier)

* FPF - First Person Flower (3D) : https://framagit.org/godot/FPS_First-Person-Flower_3D (jeux 3D ambitieux qui intégrera de nombreux développements et tutoriels autour du KinematicBody)

* Simulateur Drone 3D : https://framagit.org/godot/Drone_simulator_3D (autre jeux ambitieux qui intégrera de nombreux développements et tutoriels, plutot orienté véhicule utilisant le RigidBody)

* OpenStreetMap in Godot : https://framagit.org/godot/OpenStreetMap-3D   (pour creer une ville automatiquement en 3D à partir des cartes Open Street Map)


Le texte des fichiers à lire peut etre mis en forme avec les conventions "markdown" voir https://fr.wikipedia.org/wiki/Markdown pour exemples
    
Des explications et tutoriels relatifs à ces jeux sont disponibles sur le site collaboratif des ateliers Godot Montpel'libre :
  https://godot.tuxfamily.org/
  (pour modifier les fichier connectez vous avec votre accès sur https://godot.tuxfamily.org/admin )
  
  
    